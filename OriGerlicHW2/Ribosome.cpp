#include "Ribosome.h"

/*
Creates the protein
input: std::string& RNA_transcript
output: a new protein
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript)const
{
	Protein* prot = new Protein();
	std::string codon = "";

	prot->init();
	while (RNA_transcript.length() >= 3)
	{
		codon = RNA_transcript.substr(0, 3);
		RNA_transcript.erase(0, 3);
		if (get_amino_acid(codon) == UNKNOWN)
		{
			prot->clear();
			return nullptr;
		}
		else
		{
			prot->add(get_amino_acid(codon));
		}
	}
	return prot;
}