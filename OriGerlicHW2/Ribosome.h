#ifndef RIBOSOME_H
#define RIBOSOME_H

#include <string>
#include <iostream>
#include "Protein.h"

class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;

};

#endif // RIBOSOME_H
