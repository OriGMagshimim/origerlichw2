#include "Nucleus.h"

/*
Initiates the gene
input: const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand
output: none
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->__start = start;
	this->__end = end;
	this->__on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
Gets the start
input: none
output: start
*/
unsigned int Gene::get_start()const
{
	return this->__start;
}

/*
Gets the end
input: none
output: end
*/
unsigned int Gene::get_end()const
{
	return this->__end;
}

/*
Gets the on_complementary_dna_strand
input: none
output: on_complementary_dna_strand
*/
bool Gene::get_is_on_complementary_dna_strand()const
{
	return this->__on_complementary_dna_strand;
}

/*
Sets the start
input: const unsigned int start
output: none
*/
void Gene::set_start(const unsigned int start)
{
	this->__start = start;
}

/*
Sets the end
input: const unsigned int end
output: none
*/
void Gene::set_end(const unsigned int end)
{
	this->__end = end;
}

/*
Sets the on_complementary_dna_strand
input: bool on_complementary_dna_strand
output: none
*/
void Gene::set_on_complementary_dna_strand(bool on_complementary_dna_strand)
{
	this->__on_complementary_dna_strand = on_complementary_dna_strand;
}

/*
Initiates the DNA
input: const std::string dna_sequence
output: none
*/
void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;

	for (int i = 0; i < this->_DNA_strand.length(); i++)
	{
		if (this->_DNA_strand[i] == 'G')
			this->_complementary_DNA_strand += 'C';
		else if (this->_DNA_strand[i] == 'C')
			this->_complementary_DNA_strand += 'G';
		else if (this->_DNA_strand[i] == 'T')
			this->_complementary_DNA_strand += 'A';
		else if (this->_DNA_strand[i] == 'A')
			this->_complementary_DNA_strand += 'T';
		else
		{
			std::cerr << "Error";
			_exit(1);
		}
	}
}

/*
Creates the RNA
input: const Gene& gene
ouput: the RNA
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene)const
{
	std::string rna = "";

	if (gene.get_is_on_complementary_dna_strand())
	{
		for (int i = gene.get_start(); i < gene.get_end(); i++)
		{
			if (this->_complementary_DNA_strand[i] == 'G')
				rna += 'G';
			else if (this->_complementary_DNA_strand[i] == 'C')
				rna += 'C';
			else if (this->_complementary_DNA_strand[i] == 'T')
				rna += 'U';
			else if (this->_complementary_DNA_strand[i] == 'A')
				rna += 'A';
			else
			{
				std::cerr << "Error";
				_exit(1);
			}
		}
	}
	else
	{
		for (int i = gene.get_start(); i < gene.get_end(); i++)
		{
			if (this->_DNA_strand[i] == 'G')
				rna += 'G';
			else if (this->_DNA_strand[i] == 'C')
				rna += 'C';
			else if (this->_DNA_strand[i] == 'T')
				rna += 'U';
			else if (this->_DNA_strand[i] == 'A')
				rna += 'A';
			else
			{
				std::cerr << "Error";
				_exit(1);
			}
		}
	}

	return rna;
}

/*
Creates a reversed DNA strand
input: none
output: reversed DNA strand
*/
std::string Nucleus::get_reversed_DNA_strand()const
{
	std::string reversed_DNA_STRAND = "";

	for (int i = 0; i < this->_DNA_strand.length(); i++)
	{
		reversed_DNA_STRAND[i] = this->_DNA_strand[this->_DNA_strand.length() - i - 1];
	}

	return reversed_DNA_STRAND;
}

/*
Finds how many times a specific codon is in the DNA strand
input: const std::string& codon
output: times specific codoin is in the DNA strand
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon)const
{
	unsigned int counter = 0;
	unsigned int index = 0;

	while (this->_DNA_strand.find(codon, index) < this->_DNA_strand.length() - 1)
	{
		index = this->_DNA_strand.find(codon, index) + 3;
		counter += 1;
	}

	return counter;
}
