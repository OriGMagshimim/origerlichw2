#ifndef CELL_H
#define CELL_H

#include <string>
#include <iostream>
#include "Mitochondrion.h"

class Cell
{
public:
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();

private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};

#endif CELL_H