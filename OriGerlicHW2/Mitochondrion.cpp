#include "Mitochondrion.h"

/*
Initiates the mitochondrion
input: none
output: none 
*/
void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

/*
Checks if the glucose receptor can be inserted
input: const Protein& protein
output: none
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	Protein prot = protein;

	if (prot.get_first()->get_data() == ALANINE && prot.get_first()->get_next()->get_data() == LEUCINE && prot.get_first()->get_next()->get_next()->get_data() == GLYCINE && prot.get_first()->get_next()->get_next()->get_next()->get_data() == HISTIDINE && prot.get_first()->get_next()->get_next()->get_next()->get_next()->get_data() == LEUCINE && prot.get_first()->get_next()->get_next()->get_next()->get_next()->get_next()->get_data() == PHENYLALANINE)
		this->_has_glocuse_receptor = true;
	else
		this->_has_glocuse_receptor = false;
}

/*
Changes the glucose level
input: const unsigned int glocuse_units
output: none
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

/*
Checks if the mitochondrion can produce atp
input: none
output: if the mitochondrion can produce atp
*/
bool Mitochondrion::produceATP()const
{
	if (this->_has_glocuse_receptor == true && this->_glocuse_level >= 50)
		return true;
	return false;
}