#include "Cell.h"

/*
Initiates the cell
input: const std::string dna_sequence, const Gene glucose_receptor_gene
output: none
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
}

/*
Loads the cell with energy
input: none
output: none
*/
bool Cell::get_ATP()
{
	Protein* prot = new Protein();
	std::string new_rna = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	std::string& rna_transcript = new_rna;

	prot->init();
	prot = this->_ribosome.create_protein(rna_transcript);

	if (prot == nullptr)
	{
		std::cerr << "Error";
		_exit(1);
	}
	else
	{
		this->_mitochondrion.insert_glucose_receptor(*prot);
		this->_mitochondrion.set_glucose(50);

		if (this->_mitochondrion.produceATP() == true)
		{
			this->_atp_units;
			return true;
		}
	}
	return false;
}