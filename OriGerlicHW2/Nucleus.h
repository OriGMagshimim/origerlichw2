#ifndef NUCLEUS_H
#define NUCLEUS_H

#include <string>
#include <iostream>

class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	unsigned int get_start()const;
	unsigned int get_end()const;
	bool get_is_on_complementary_dna_strand()const;
	void set_start(const unsigned int start);
	void set_end(const unsigned int end);
	void set_on_complementary_dna_strand(bool on_complementary_dna_strand);


private:
	unsigned int __start;
	unsigned int __end;
	bool __on_complementary_dna_strand;
};

class Nucleus
{
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;

};

#endif // NUCLEUS_H
